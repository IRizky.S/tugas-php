<?php

require_once('Animal.php');
//require('frog.php');
//require('ape.php');


$sheep = new Animal("Shaun");

echo "Nama Hewan:  $sheep->name <br>"; 
echo "Jumlah Kaki:  $sheep->legs <br>";
echo "Cold blooded:  $sheep->cold_blooded <br> <br>";

require_once('frog.php');
$kodok = new frog("Buduk");

echo "Nama Hewan: $kodok->name <br>"; 
echo "Jumlah Kaki: $kodok->legs <br>";
echo "Jump: $kodok->jump <br> <br>";

require_once('ape.php');
$kera = new ape("Kera Sakti");

echo "Nama Hewan: $kera->name <br>"; 
echo "Jumlah Kaki: $kera->legs <br>";
echo "Bersuara: $kera->yell <br> <br>";